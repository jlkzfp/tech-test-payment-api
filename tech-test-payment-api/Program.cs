using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(options =>
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
	
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c =>
		{
			c.RouteTemplate = "/api-docs/{documentName}/swagger.json";
		});
    app.UseSwaggerUI(c =>
		{
			c.SwaggerEndpoint($"/api-docs/v1/swagger.json", $"APP API - V1");
			c.RoutePrefix = "api-docs";
		});
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
