using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PaymentAPI.Interfaces;

namespace PaymentAPI.Entities
{
    public class Venda : IValidacao
    {
		public static readonly List<Venda> VendasRealizadas = new List<Venda>();

        public int Id { get; set; }
		public List<ItemVenda> Itens { get; set; }
		public Vendedor Vendedor { get; set; }
		private StatusEnum? Status { get; set; } = null;
		public string DescricaoStatus {
			get {
				switch (Status) {
					case StatusEnum.Aguardando_Pagamento:
						return "Aguardando Pagamento";
					case StatusEnum.Pagamento_Aprovado:
						return "Pagamento Aprovado";
					case StatusEnum.Enviado_Para_Transportadora:
						return "Enviado para transportadora";
					case StatusEnum.Entregue:
						return "Entregue";
					case StatusEnum.Cancelada:
						return "Cancelada";
					default:
						return "";
				}
			}
		}

		public bool Validar()
		{
			return this.Vendedor.Validar() &&
				this.Itens.Count > 0;
		}

		public bool AguardandoPagamento() {
			if (this.Status != null) return false;
			this.Status = StatusEnum.Aguardando_Pagamento;
			return true;
		}

		public static void SetId(Venda venda) {
			if (venda.Id > 0) return;
			int max = 0;
			if (VendasRealizadas.Count > 0) max = VendasRealizadas.Max(x => x.Id);

			venda.Id = max + 1;
		}

		public static bool AdicionarVenda(Venda venda) {
			if (
				!venda.Validar() ||
				!venda.AguardandoPagamento()
			) return false;

			SetId(venda);

			VendasRealizadas.Add(venda);

			return true;
		}

		public static Venda? ObterPorId(int id) {
			return VendasRealizadas.FirstOrDefault(x => x.Id == id, null);
		}

		public bool AtualizarStatus(StatusEnum proximo) {
			StatusEnum[] depoisAguardandoPagamento = { StatusEnum.Pagamento_Aprovado, StatusEnum.Cancelada };
			StatusEnum[] depoisPagamentoAprovado = { StatusEnum.Enviado_Para_Transportadora, StatusEnum.Cancelada };
			StatusEnum[] depoisEnviadoTransportadora = { StatusEnum.Entregue };
			StatusEnum[] statusPossiveis = null;

			switch (this.Status)  {
				case StatusEnum.Aguardando_Pagamento:
					statusPossiveis = depoisAguardandoPagamento;
					break;
				case StatusEnum.Pagamento_Aprovado:
					statusPossiveis = depoisPagamentoAprovado;
					break;
				case StatusEnum.Enviado_Para_Transportadora:
					statusPossiveis = depoisEnviadoTransportadora;
					break;
				default:
					break;
			}

			if (statusPossiveis != null && !statusPossiveis.Contains(proximo)) return false;

			this.Status = proximo;

			return true;
		}
		
    }
}