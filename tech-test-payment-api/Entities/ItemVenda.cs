using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PaymentAPI.Interfaces;

namespace PaymentAPI.Entities
{
    public class ItemVenda : IValidacao
    {
        public int IdItem { get; set; }
		public decimal Quantidade { get; set; }

		public ItemVenda(int idItem = 0, decimal quantidade = 0)
		{
			this.IdItem = idItem;
			this.Quantidade = quantidade;
		}

		public bool Validar()
		{
			return this.IdItem > 0 && this.Quantidade > 0;
		}
	}
}