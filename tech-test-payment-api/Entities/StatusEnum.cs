using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentAPI.Entities
{
    public enum StatusEnum
    {
		Aguardando_Pagamento,
        Pagamento_Aprovado, 
		Enviado_Para_Transportadora, 
		Entregue, 
		Cancelada
    }
}