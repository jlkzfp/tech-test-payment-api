using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PaymentAPI.Interfaces;

namespace PaymentAPI.Entities
{
    public class Vendedor : IValidacao
    {
		public string CPF { get; set; }
		public string Nome { get; set; }
		public string Email { get; set; }
		public string Telefone { get; set; }

		public Vendedor(string cpf = "", string nome = "", string email = "", string telefone = "")
		{
			this.CPF = cpf;
			this.Nome = nome;
			this.Email = email;
			this.Telefone = telefone;
		}

		public bool Validar()
		{
			return this.CPF.Replace(".", "").Replace("-", "").Length == 11 &&
				this.Email.Contains("@");
		}
	}
}