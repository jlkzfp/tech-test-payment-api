using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Entities;

namespace PaymentAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
    public class VendaController : ControllerBase
    {
		[HttpGet]
		public IActionResult Vendas() {
			return Ok(Venda.VendasRealizadas);
		}

		[HttpGet("{id}")]
        public IActionResult BuscarVenda(int id) {
			Venda venda = Venda.ObterPorId(id);
			if (venda == null) return BadRequest();
			return Ok(venda);
		}

		[HttpPost]
		public IActionResult Cadastrar(Venda venda) {

			if (!Venda.AdicionarVenda(venda)) return BadRequest();
			
			return Ok(venda);
		}

		[HttpPatch("AtualizarStatus/{id}")]
		public IActionResult AtualizarStatus(int id, StatusEnum proximo) {

			Venda venda = Venda.ObterPorId(id);
			if (
				venda == null ||
				!venda.AtualizarStatus(proximo)
			) return BadRequest();

			return Ok(venda);
		}
    }
}