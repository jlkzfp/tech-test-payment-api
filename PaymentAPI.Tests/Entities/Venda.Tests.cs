using PaymentAPI.Entities;

public class VendaTests
{
	
	[Fact]
	public void EntityVenda_Validar_True() {
		Venda venda = new Venda();
		venda.Vendedor = new Vendedor(cpf: "12345678900", nome: "Vendedor Fulano" ,email: "vendedor@empresa.com", telefone: "00 12345 1234");
		venda.Itens = new List<ItemVenda> {
			new ItemVenda(idItem: 2, quantidade: 10),
			new ItemVenda(idItem: 3, quantidade: 2.8M)
		};

		bool resultado = venda.Validar();

		Assert.True(resultado);
	}

	[Fact]
	public void EntityVenda_Validar_False() {
		Venda venda = new Venda();
		venda.Vendedor = new Vendedor(cpf: "12345678900", nome: "Vendedor Fulano", email: "vendedor", telefone: "00 12345 1234");
		venda.Itens = new List<ItemVenda> {
			new ItemVenda(idItem: 2, quantidade: 10),
			new ItemVenda(idItem: 3, quantidade: 2.8M)
		};

		bool resultado = venda.Validar();

		venda.Itens.Clear();
		bool resultado_1 = venda.Validar();

		Assert.False(resultado);
		Assert.False(resultado_1);
	}
}