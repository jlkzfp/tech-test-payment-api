namespace PaymentAPI.Tests;
using PaymentAPI.Entities;

public class ItemVendaTests
{
    [Fact]
    public void ModeloItemVenda_Validar_True()
    {
		ItemVenda item = new ItemVenda(idItem: 10, quantidade: 1.7M);

		bool resultado = item.Validar();

		Assert.True(resultado);
    }

	[Fact]
	public void ModeloItemVenda_Validar_False() {
		ItemVenda item = new ItemVenda(idItem: 0, quantidade: 1.7M);
		ItemVenda item_1 = new ItemVenda(idItem: 2, quantidade: 0);
		ItemVenda item_2 = new ItemVenda();

		bool resultado = item.Validar();
		bool resultado_1 = item_1.Validar();
		bool resultado_2 = item_2.Validar();

		Assert.False(resultado);
		Assert.False(resultado_1);
		Assert.False(resultado_2);
	}
}