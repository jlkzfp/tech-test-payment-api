using PaymentAPI.Entities;

public class VendedorTests
{
	
	[Fact]
	public void EntityVendedor_Validar_True() {
		Vendedor vendedor = new Vendedor(cpf: "12345678900", nome: "Vendedor Teste", email: "email.vendedor@teste.com", telefone: "(00) 12345 6789");
		Vendedor vendedor_01 = new Vendedor(cpf: "123.456.789-00", nome: "Vendedor Teste", email: "email.vendedor@teste.com", telefone: "00123456789");

		bool resultado = vendedor.Validar();
		bool resultado_01 = vendedor_01.Validar();

		Assert.True(resultado);
		Assert.True(resultado_01);
	}

	[Fact]
	public void EntityVendedor_Validar_False() {
		Vendedor vendedor = new Vendedor(cpf: "123456789", nome: "Vendedor Teste", email: "email.vendedor@teste.com", telefone: "(00) 12345 6789");
		Vendedor vendedor_01 = new Vendedor(cpf: "123.456.789-00", nome: "Vendedor Teste", email: "email.vendedor", telefone: "00123456789");

		bool resultado = vendedor.Validar();
		bool resultado_01 = vendedor_01.Validar();

		Assert.False(resultado);
		Assert.False(resultado_01);
	}
}
